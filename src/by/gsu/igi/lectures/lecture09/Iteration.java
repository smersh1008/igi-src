package by.gsu.igi.lectures.lecture09;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Created by Evgeniy Myslovets.
 */
public class Iteration {

    public static void main(String[] args) {
        demo(new HashSet());
        demo(new LinkedHashSet());
        demo(new TreeSet());
        demo(new TreeSet(new DescendingComparator()));
        demo(new LinkedList());
        demo(new ArrayList());
    }

    private static void demo(Collection c) {
        System.out.println(c.getClass().getSimpleName());
        c.add("C");
        c.add("A");
        c.add("B");
        c.add("D");
        c.add("A");

        Iterator iterator = c.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    private static class DescendingComparator implements Comparator<String> {
        public int compare(String o1, String o2) {
            return -o1.compareTo(o2);
        }
    }
}