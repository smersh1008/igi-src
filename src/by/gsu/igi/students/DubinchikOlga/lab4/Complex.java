package by.gsu.igi.students.DubinchikOlga.lab4;

/**
 * Created by NotePad.by.
 */
public class Complex {
    private double real;
    private double imaginaries;
    private double module;
    private double corner;


    public static void main(String[] args) {
        Complex z = new Complex(2, 5);
        Complex q = new Complex(2, 3);
       // System.out.println(z);
      //  System.out.println(q);
        System.out.println("sum:");
        System.out.println(z.add(q));
        System.out.println("difference:");
        System.out.println(z.substract(q));
        System.out.println("multiplication:");
        System.out.println(z.multiply(q));
        System.out.println("division:");
        System.out.println(z.divide(q));
    }

    public Complex(double real, double imaginaries) {
        this.real = real;
        this.imaginaries = imaginaries;
        module = Math.sqrt(real * real + imaginaries * imaginaries);
        fixModuleAndCorner(real, imaginaries);
    }

    public Complex() {
        this(0, 0);
    }

    public double getReal() {
        return real;
    }

    public double getImaginaries() {
        return imaginaries;
    }

    public Complex add(Complex complex) {
        return new Complex(real + complex.getReal(), imaginaries + complex.getImaginaries());
    }

    public Complex substract(Complex complex) {
        return new Complex(real - complex.getReal(), imaginaries - complex.getImaginaries());
    }

    public Complex multiply(Complex complex) {
        double real = this.real * complex.getReal() - this.imaginaries * complex.getImaginaries();
        double imaginaries = real * complex.getImaginaries() + this.imaginaries * complex.getReal();
        return new Complex(real, imaginaries);
    }

    public Complex divide(Complex q) {
        double real = (this.real * q.getReal() + this.imaginaries * q.getImaginaries()) / (this.real * this.real + q.getReal() * q.getReal());
        double imaginaries = (this.imaginaries * q.getReal() - this.real * q.getImaginaries()) / (this.real * this.real + q.getReal() * q.getReal());
        return new Complex(real, imaginaries);
    }

    @Override
    public String toString() {
        String s = showAlgebraic() + "\n" + showTrigonometric();
        return s;
    }

    public String showAlgebraic() {
        return "z = " + real + " + i*" + imaginaries;
    }

    public String showTrigonometric() {
        return "z = " + module + "*(cos(" + corner + ") + sin(" + corner + "))";
    }

    private void fixModuleAndCorner(double real, double imaginaries) {
        if (real != 0) {
            corner = Math.atan(imaginaries / real);
        } else {
            if (imaginaries != 0) {
                corner = Math.signum(real) * (Math.PI / 2);
            } else {
                corner = 0;
            }

        }
    }
}
